# Frontend Mentor - FAQ accordion card solution

This is a solution to
the [FAQ accordion card challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/faq-accordion-card-XlyjD0Oam)
. Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
    - [The challenge](#the-challenge)
    - [Screenshot](#screenshot)
    - [Links](#links)
- [My process](#my-process)
    - [Built with](#built-with)
    - [What I learned](#what-i-learned)
    - [Useful resources](#useful-resources)
- [Author](#author)

## Overview

### The challenge

Users should be able to:

- View the optimal layout for the component depending on their device's screen size
- See hover states for all interactive elements on the page
- Hide/Show the answer to a question when the question is clicked

### Screenshot

![](./desktop_screenshot.png)
![](./mobile_screenshot.png)

### Links

- Solution URL: [Gitlab](https://gitlab.com/practice-projects2/frontendmentor/faq-accordion-card)
- Live Site URL: [Netlify](https://unruffled-albattani-e37254.netlify.app/)

## My process

1. set up react project with create-react-app
2. create accordion component
3. populate app.js with accordions
4. mobile layout
5. desktop layout

### Built with

- Flexbox
- Mobile-first workflow
- [React](https://reactjs.org/) - JS library
- [Styled Components](https://styled-components.com/) - For styles

### What I learned

Flip the arrow vertically instead of rotating 180 degree

```html
scaleY(-1)
```

### Useful resources

- [CSS Variables for React Devs](https://www.joshwcomeau.com/css/css-variables-for-react-devs/)
- [Styled Components - styling based on style props/component states](https://styled-components.com/docs/basics#adapting-based-on-props)
- [The Picture Element](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/picture)
- [Using Media Queries in Styled Components](https://www.mariokandut.com/how-to-use-media-queries-in-styled-components/)

## Author

- Github - [cherylli](https://github.com/cherylli)
- Gitlab - [cherylm](https://gitlab.com/cherylm)
- Frontend Mentor - [@cherylli](https://www.frontendmentor.io/profile/cherylli)


