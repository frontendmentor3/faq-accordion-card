import Accordion from "./components/Accordion";
import styled from "styled-components";
import illustrationMobile from './images/illustration-woman-online-mobile.svg'
import bgMobile from './images/bg-pattern-mobile.svg'
import illustrationDesktop from './images/illustration-woman-online-desktop.svg'
import bgDesktop from './images/bg-pattern-desktop.svg'
import boxDesktop from './images/illustration-box-desktop.svg'
import {devices} from "./constants/devices";

const AppStyles = styled.div`
  background: linear-gradient(180deg, var(--soft-violet), var(--soft-blue));
  height: 100vh;
  display: flex;
  flex-direction: row;
`

const CardStyles = styled.div`
  width: 80%;
  max-width: 700px;
  min-width: 275px;
  padding: 7rem 1rem 2rem 1rem;
  margin: auto;
  background-color: var(--white);
  border-radius: 20px;
  filter: drop-shadow(0 20px 50px var(--shadow));
  display: flex;
  flex-direction: column;
  align-items: center;
  position: absolute;
  left: 0;
  right: 0;
  top: 150px;
  z-index: 1000;

  .card__image {
    position: absolute;
    top: -105px;
    width: 300px;
    height: 300px;
    background-image: url(${illustrationMobile}), url(${bgMobile});
    background-repeat: no-repeat;
    background-size: 80%, 80%;
    background-position: 30px 0px, 30px 100px;
  }

  .logo-bg {
    position: relative;
    top: -80px;
    z-index: -1;
  }

  @media ${devices.desktop} {
    align-items: flex-end;
    padding: 2rem 2rem 4rem 0;

    .card__image {
      position: absolute;
      width: 400px;
      height: 600px;
      left: -50px;
      top: 50px;
      background-image: url(${boxDesktop}), url(${illustrationDesktop}), url(${bgDesktop});
      background-size: 40%, 100%, 100%;
      background-position: -40px 160px, 0px 50px, 0px 30px;
    }


  }
`

const TitleStyles = styled.h1`
  text-align: center;
  font-size: 2rem;
  @media ${devices.desktop} {
    text-align: left;
    font-size: 1.5rem
  }
`

function App() {
    const qnaList = [
        {
            question: "How many team members can I invite?",
            answer: "You can invite up to 2 additional users on the Free plan. There is no limit on team members for the Premium plan.",
        }, {
            question: "What is the maximum file upload size?",
            answer: "No more than 2GB. All files in your account must fit your allotted storage space.",
        }, {
            question: "How do I reset my password?",
            answer: "Click “Forgot password” from the login page or “Change password” from your profile page. A reset link will be emailed to you.",
        }, {
            question: "Can I cancel my subscription?",
            answer: "Yes! Send us a message and we’ll process your request no questions asked.",
        }, {
            question: " Do you provide additional support?",
            answer: "Chat and email support is available 24/7. Phone lines are open during normal business hours.}",
        }
    ]

    return (
        <AppStyles className="App">
            <CardStyles>
                <section className="card__image">

                </section>
                <section>
                    <TitleStyles>FAQ</TitleStyles>

                    {qnaList.map((item, i) => (
                        <Accordion question={item.question}
                                   answer={item.answer}
                                   key={i}
                        />
                    ))}
                </section>
            </CardStyles>

        </AppStyles>
    );
}

export default App;
