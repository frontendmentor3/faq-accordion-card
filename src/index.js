import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css'
import {createGlobalStyle} from "styled-components";

const GlobalStyles = createGlobalStyle`
  :root {
    // Primary text
    --very-dark-desaturated-blue: hsl(238, 29%, 16%);
    --soft-red: hsl(14, 88%, 65%);
    // background gradient
    --soft-violet: hsl(273, 75%, 66%);
    --soft-blue: hsl(240, 73%, 65%);
    // text
    --very-dark-grayish-blue: hsl(237, 12%, 33%);
    --dark-grayish-blue: hsl(240, 6%, 50%);
    // divider
    --light-grayish-blue: hsl(240, 5%, 91%);
    // misc
    --white: hsl(0, 100%, 100%);
    --shadow: hsl(240, 73%, 35%);
  }
`

ReactDOM.render(
    <React.StrictMode>
        <GlobalStyles/>
        <App/>

    </React.StrictMode>,
    document.getElementById('root')
);

