import React, {useState} from "react";
import downArrow from '../images/icon-arrow-down.svg'
import styled from 'styled-components'
import {devices} from "../constants/devices";

const Wrapper = styled.div`
  max-width: 400px;
  @media ${devices.desktop} {
    padding-right: 50px;
  }
`

const QuestionStyles = styled.div`
  color: ${props => props.opened ? "black" : "var(--very-dark-grayish-blue)"};
  font-weight: ${props => props.opened ? "bold" : "normal"};
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 1rem 0;
  max-width: 350px;

  &:hover {
    color: var(--soft-red);
    cursor: pointer;
  }

`

const AnswerStyles = styled.div`
  color: var(--dark-grayish-blue);
  padding-bottom: 1rem;
  max-width: 230px;
  //margin-right: 150px;
  //padding-right: 50px;
`

const DividerStyles = styled.hr`
  color: var(--light-grayish-blue);
`

const ArrowStyles = styled.img`
  padding-left: 10px;
  display: block;
  margin-left: auto;
  transform: ${props => props.opened && "scaleY(-1)"};

`

const Accordion = ({question, answer}) => {

    const [isOpen, setIsOpen] = useState(false)

    return (
        <Wrapper>
            <QuestionStyles opened={isOpen} onClick={() => setIsOpen(!isOpen)}>
                {question}
                <ArrowStyles opened={isOpen} alt="open" src={downArrow}/>
            </QuestionStyles>
            {isOpen &&
            <AnswerStyles>
                {answer}
            </AnswerStyles>
            }
            <DividerStyles/>
        </Wrapper>
    )
}

export default Accordion