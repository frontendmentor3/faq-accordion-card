const SIZES = {
    mobile: '780px',
}

export const devices = {
    desktop: `(min-width:${SIZES.mobile})`
}